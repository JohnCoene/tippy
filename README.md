[![Build Status](https://travis-ci.org/JohnCoene/tippy.svg?branch=master)](https://travis-ci.org/JohnCoene/tippy) [![CRAN status](https://www.r-pkg.org/badges/version/tippy)](https://cran.r-project.org/package=tippy) [![](https://cranlogs.r-pkg.org/badges/tippy)](https://cran.r-project.org/package=tippy)

# tippy

![tippy](/man/figures/logo.png)

[Tippy.js](https://atomiks.github.io/tippyjs) R htmlwidget.

## Install

```r
# stable
install.packages("tippy") 

# dev
devtools::install_github("JohnCoene/tippy")
devtools::install_bitbucket("JohnCoene/tippy")
```

### [Documentation](http://tippy.john-coene.com/)
